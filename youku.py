#!/usr/bin/python
#coding=utf-8
from Tkinter import *
from fcflv import *
import tkMessageBox

'''
执行主脚本， 使用Tk库做UI
@author garethxiao@qq.com
'''
class App:
    def __init__(self, master):
        master.title("优酷视频抓取")
        frame = Frame(master)
        frame.pack()     
        #文本
        v1 = StringVar()
        v2 = StringVar()
        v1.set("")
        v2.set("")
        Label(frame, text="单个视频地址", width=100, height=1, 
                wraplength=100,justify="left").pack()        
        #默认情况下下Entry的状态为normal
        #单个视频URL输入框
        self.entry = Entry(frame, width=100, textvariable=v1)
        self.entry.pack()
        #按钮
        self.single_button = Button(frame, text=u"下载单个flv视频", fg="blue", command=self.doFcflv)
        self.single_button.pack()
        Label(frame, text="电视剧频道页地址", width=100, height=1, 
                wraplength=100, justify="left").pack()          
        #电视剧频道URL输入框
        self.entry2 = Entry(frame, width=100, textvariable=v2)
        self.entry2.pack()       
        self.single_button = Button(frame, text=u"下载电视剧", command=self.doTVseries)
        self.single_button.pack(side=LEFT)        
        self.quit_button = Button(frame, text=u"退出", command=frame.quit)
        self.quit_button.pack(side=RIGHT)
        Label(root, text="garethxiao@qq.com", width=120, height=2, 
                wraplength=120, justify="left").pack()

    def mycallback():
        tkMessageBox.showinfo("运行提示","下载完成！")

    def doFcflv(self):
        v1 = StringVar()

        v1.set("请稍等， 正在下载中.......")
        message = Entry(root, textvariable=v1, bg="white", state="disabled",width=120)
        message.pack()

        fcflv = FCFLV()                
        status = fcflv.parse(self.entry.get())        
        #status = fcflv.parse('http://v.youku.com/v_show/id_XNTU3NjA5NTAw.html')
        if status == True :
            v1.set("下载完成！")
            print 'finish'
        else:
            v1.set("下载失败！")
            print 'False'

    def doTVseries(self):
        v1 = StringVar()

        v1.set("请稍等， 正在下载中.......")
        message = Entry(root, textvariable=v1, bg="white", state="disabled", width=120)
        message.pack()

        fcflv = FCFLV()                
        status = fcflv.fcFLV(self.entry2.get())
        if status == True :
            v1.set("下载完成！")
            print 'finish'
        else:
            v1.set("下载失败！")
            print 'False'
 
if __name__ == "__main__":
    root = Tk()
    app = App(root)
    root.mainloop()
    root.destroy()
